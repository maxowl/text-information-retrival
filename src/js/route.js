import boolean from '../modules/overview/boolean'
import cosine from '../modules/overview/cosine'

export default [
    {
        path: '/',
        redirect: '/boolean'
    },
    {
        path: '/boolean',
        name: 'boolean',
        component: boolean
    },
    {
        path: '/cosine',
        name: 'cosine',
        component: cosine
    },
];