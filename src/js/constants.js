// This file store web global constants

// Return the clone of whole constants
window.consts = () => JSON.parse(JSON.stringify({
    roomType: {
        'lab': 'Laboratory',
        'lec': 'Lecture'
    },

    role: ['Admin','Teacher','Student'],

    permission: {
        'bookRoom': ['Admin', 'Teacher', 'Student'],
    },

    building: {
        'Witsawawatthana Building': [10, 11, 12],
        'CB4': [1, 2, 3, 4, 5]
    }
}));