import refRoute from './route'

export default [
    {
        // icon: 'mdi-view-dashboard',
        label: 'Boolean search',
        path: {
            name: 'boolean',
            alias: ['boolean']
        }
    },
    {
        // icon: 'mdi-view-list',
        label: 'cosine similarity search',
        path: {
            name: 'cosine'
        }
    },

];
