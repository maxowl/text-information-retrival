// JavaScript using for whole app such as enable-disable form function

/**
 * Global function section
 */

// Set web's title.
window.setPageTitle = (title) => {
	$('title').text(title);
}

// Check if the role has given permission or not.
window.hasPermission = (role, perm) => {
	const perms = window.consts().permission;

	if (perms.hasOwnProperty(perm)) {
		return perms[perm].includes(role);
	} else {
		return false;
		// throw `Error: Permission ${perm} not found`;
	}
}

window.handleFirebaseError = (error) => {
	let errorCode = error.code;
    let errorMessage = error.message;

    console.error(`Error code: ${errorCode}, ${errorMessage}`);
}

window.genBuildingFilter = () => {
	const buildings = window.consts().building;
	let i = 0,
		j = 0,
		filter = {
			label: 'Select filter',
			data: {}
		};
	
	for (let building in buildings) {
		let floors = buildings[building]
		j = 0;
	
		filter.data[i] = {
			label: building,
		}
	
		if (floors.length) {
			filter.data[i].data = {}
		} else {
			filter.data[i].action = (v) => { 
				v.$emit('callFilter', {
					building: building,
					floor: 1
				})
			}
		}
	
		for (j in floors) {
			let floor = floors[j]
			filter.data[i].data[j] = {
				label: `Floor ${floor}`,
				action: (v) => { 
					v.$emit('callFilter', {
						building: building,
						floor: floor
					})
				}
			}
		}
	
		i++;
	}

	return filter;

}

window.genWeekLabel = (start, end) => {
	let days = [start.format('D'), end.format('D')];
	let month = start.format('MMMM');
	let months = [start.format('MMM'), end.format('MMM')];
	let years = [start.format('YYYY'), end.format('YYYY')];
	let label = ""

	if (months[0] === months[1]) {
		// 21 - 27 October 2018
		label = `${days[0]} - ${days[1]} ${month} ${years[0]}`
	} else if ((months[0] !== months[1]) && (years[0] === years[1])) {
		// 28 Oct - 3 Nov 2018
		label = `${days[0]} ${months[0]} - ${days[1]} ${months[1]} ${years[0]}`
	} else if ((months[0] !== months[1]) && (years[0] !== years[1])) {
		// 30 Dec 2018 - 5 Jan 2018
		label = `${days[0]} ${months[0]} ${years[0]} - ${days[1]} ${months[1]} ${years[1]}`
	}

	return label
}



/**
 * Global event register
 */
$(document).ready(function () {
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})
});

