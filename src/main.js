// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueFire from 'vuefire'
import './js/constants.js'
import './js/default.js'

// LoDash
import _ from 'lodash'

// Router
import VueRouter from 'vue-router'
import routes from './js/route.js'

// Calendar
import FullCalendar from 'vue-full-calendar'
import "fullcalendar/dist/fullcalendar.min.css"

// Bootstrap
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Notification
import Notifications from 'vue-notification'

// Materialize icons
import './assests/icons/materialize/css/materialdesignicons.min.css'

Vue.use(VueFire)
Vue.use(VueRouter)
Vue.use(FullCalendar)
Vue.use(BootstrapVue)
Vue.use(Notifications)

Vue.config.productionTip = false

window.router = new VueRouter({routes})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  components: { App },
  template: '<App/>',
})
