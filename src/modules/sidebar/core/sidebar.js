export function pageExist (page, menus) {
    for (let i in menus) {
        let menu = menus[i]

        if (menu.hasOwnProperty('path')) {
            if (page == menu.path.name) {
                return true;
            }
        }

        if (menu.hasOwnProperty('subMenu')) {
            if (pageExist(page, menu.subMenu)) {
                return true;
            }
        }
    }

    return false;
}