import firebase from 'firebase'

firebase.initializeApp({
// Populate your firebase configuration data here.
apiKey: "AIzaSyBtTuKtc2viBnAblCzM_pWfpWL_NzUKhtk",
authDomain: "kmuttrrtest.firebaseapp.com",
databaseURL: "https://kmuttrrtest.firebaseio.com/",
projectId:"kmuttrrtest",
storageBucket:"kmuttrrtest.appspot.com"
// storageBucket: "...",
// messagingSenderId: "..."
});

window.firebase=firebase;
window.db=firebase.firestore();

db.settings({
	timestampsInSnapshots: true
});