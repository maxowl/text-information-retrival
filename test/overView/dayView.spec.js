
import dayView from "../../src/modules/overView/dayView.vue"
let moment = require('moment');

describe('dayView.vue',function(){
    
    it('checking default booking time data', () => {
        expect(typeof dayView.data).toBe('function')
        const defaultData = dayView.data()
        let emptyArray = []
        expect(emptyArray).toEqual(defaultData.bookingTime )
    })

    it('dayView can get Booking Time', () => {
        let testTime = 8
        let expectTime = "08:00"
        expect(typeof dayView.methods.getBookingTime).toEqual('function')
        expect(expectTime).toEqual(dayView.methods.getBookingTime(testTime))
    })

    it('dayView can get current date', () => {
        let expectedCurrentDate = moment().format('D MMM, YYYY')

        expect(typeof dayView.computed.currentDate).toEqual('function')
        expect(expectedCurrentDate).toEqual(dayView.computed.currentDate())
    })

    it('dayView can get current date query format', () => {
        let expectedCurrentDateQueryFormat = moment().format('YYYY-MM-DD')

        expect(typeof dayView.computed.currentDateQueryFormat).toEqual('function')
        expect(expectedCurrentDateQueryFormat).toEqual(dayView.computed.currentDateQueryFormat())
    })

    it('dayView can get eightToTwentyfour array', () => {
        let expectedArray = [ 8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 ]

        expect(typeof dayView.computed.eightToTwentyfour).toEqual('function')
        expect(expectedArray).toEqual(dayView.computed.eightToTwentyfour())
    })

})