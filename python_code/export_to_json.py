import json
import pandas as pd 
from nltk.stem.porter import PorterStemmer
import re
import operator
porter_stemmer = PorterStemmer()
data = pd.read_csv('songdata.csv')

dataNew = data.to_json(orient="records")

with open('dict.json','w') as outfile:
	json.dump(dataNew, outfile)

with open('song.json','w') as songFile:
	json.dump(list(data['song']), songFile)

processData = []
symbol = "!@#$%^&*()_+=-~|\\}]{[\'\";:/>?<,"
result = []
countWord = {}
for i in data['text']:
	i = i.lower().replace("let's","let us")
	i = i.lower().replace("'s"," is")
	i = i.lower().replace("'m"," am")
	i = i.lower().replace("'ll"," will")
	i = i.lower().replace("don't","do not")
	i = i.lower().replace("doesn't","does not")
	i = i.lower().replace("'re'"," are")
	i = i.lower().replace("'ve'"," have")
	i = i.lower().replace("hasn't","has not")
	i = i.lower().replace("haven't","have not")
	i = i.lower().replace("shouldn't","should not")
	i = i.lower().replace("can't","can not")
	i = i.lower().replace("couldn't","could not")
	i = i.lower().replace("\n"," ")
	i = re.sub('[^a-zA-Z0-9]',' ',i)
	processData.append(i.lower().split())
for words in processData:
	ordered_tokens = set()
	ordered_list = []
	for word in words:
		if countWord.get(word):
			countWord[word] = countWord[word] + 1
		else:
			countWord[word] = 1
		if word not in ordered_tokens:
			ordered_tokens.add(word)
			ordered_list.append(word)
	result.append(sorted(ordered_list))
sorted_d = sorted(countWord.items(),key=operator.itemgetter(1),reverse=True)
word_stopWord = []
for i in sorted_d[:30]:
	word_stopWord.append(i[0])

with open('stopWord.json','w') as stopWordFile:
	json.dump(word_stopWord, stopWordFile)

dataCountStem = {}
dataStem = {}
for index, row in data.iterrows():
	i = row['text']
	i = i.lower().replace("let's","let us")
	i = i.lower().replace("'s"," is")
	i = i.lower().replace("'m"," am")
	i = i.lower().replace("'ll"," will")
	i = i.lower().replace("don't","do not")
	i = i.lower().replace("doesn't","does not")
	i = i.lower().replace("'re'"," are")
	i = i.lower().replace("'ve'"," have")
	i = i.lower().replace("hasn't","has not")
	i = i.lower().replace("haven't","have not")
	i = i.lower().replace("shouldn't","should not")
	i = i.lower().replace("can't","can not")
	i = i.lower().replace("couldn't","could not")
	i = i.lower().replace("\n"," ")
	i = re.sub('[^a-zA-Z0-9]',' ',i)
	processDataframe = i.lower().split()
	ordered_tokens = set()
	ordered_list = []
	countWordTmp = {}
	for word in processDataframe:
		if countWordTmp.get(word):
			countWordTmp[word] = countWordTmp[word] + 1
		else:
			countWordTmp[word] = 1
			if dataStem.get(word):
				dataStem[word].append(str(index+1))
			else:
				dataStem[word] = list()
				dataStem[word].append(str(index+1))
		if word not in ordered_tokens:
			ordered_tokens.add(word)
			ordered_list.append(word)
	dataCountStem[row['song']] = [countWordTmp]

with open('dictCountStem.json','w') as countStemFile:
	json.dump(dataCountStem, countStemFile)

with open('dictStem.json','w') as stemFile:
	json.dump(dataStem, stemFile, sort_keys = True)